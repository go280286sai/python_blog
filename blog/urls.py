from django.urls import path

import blog
from blog import views

urlpatterns = [
    path('', blog.views.index, name="home"),
]
